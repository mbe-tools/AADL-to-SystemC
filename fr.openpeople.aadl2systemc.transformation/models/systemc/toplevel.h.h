//AADL specification(video_software_appli.aadl) translated into C++ (/fr.openpeople.aadl2systemc.transformation/models/systemc/toplevel.h.h)
#include "aadl.h"
namespace AADL_video_software_appli {

  namespace video_software_appli {
    typedef VGAPixel vgapixeltype;
  }

  namespace video_software_appli {
    typedef ImageId imageidtype;
  }

  namespace video_software_appli {
    class synchrot : public AADL::threadType {
      public: 
        AADL::eventDataPort_out<video_software_appli::imageidtype> capture_start;
        AADL::eventDataPort_out<video_software_appli::imageidtype> processing_start;
        AADL::eventDataPort_out<video_software_appli::imageidtype> display_start;
        AADL::eventPort_in<bool> capture_done;
        AADL::eventPort_in<bool> capture_ready;
        AADL::eventPort_in<bool> processing_done;
        AADL::eventPort_in<bool> processing_ready;
        AADL::eventPort_in<bool> display_done;
        AADL::eventPort_in<bool> display_ready;
      public:
        synchrot(AADL::moduleName name) : AADL::threadType(name) {
        }
    };
  }

  namespace video_software_appli {
    class capturet : public AADL::threadType {
      public: 
        AADL::eventDataPort_in<video_software_appli::imageidtype> start;
        AADL::eventDataPort_in<video_software_appli::vgapixeltype> pixel;
        AADL::eventPort_out<bool> done;
        AADL::eventPort_out<bool> ready;
      public:
        capturet(AADL::moduleName name) : AADL::threadType(name) {
        }
    };
  }

  namespace video_software_appli {
    class processingt : public AADL::threadType {
      public: 
        AADL::eventDataPort_in<video_software_appli::imageidtype> start;
        AADL::eventPort_out<bool> done;
        AADL::eventPort_out<bool> ready;
      public:
        processingt(AADL::moduleName name) : AADL::threadType(name) {
        }
    };
  }

  namespace video_software_appli {
    class displayt : public AADL::threadType {
      public: 
        AADL::eventDataPort_in<video_software_appli::imageidtype> start;
        AADL::eventDataPort_out<video_software_appli::vgapixeltype> pixel;
        AADL::eventPort_out<bool> done;
        AADL::eventPort_out<bool> ready;
      public:
        displayt(AADL::moduleName name) : AADL::threadType(name) {
        }
    };
  }

  namespace video_software_appli {
    typedef My_Synchro synchrot_DOT_impl;
  }

  namespace video_software_appli {
    class synchrop : public AADL::processType {
      public: 
        AADL::eventDataPort_out<video_software_appli::imageidtype> capture_start;
        AADL::eventDataPort_out<video_software_appli::imageidtype> processing_start;
        AADL::eventDataPort_out<video_software_appli::imageidtype> display_start;
        AADL::eventPort_in<bool> capture_done;
        AADL::eventPort_in<bool> capture_ready;
        AADL::eventPort_in<bool> processing_done;
        AADL::eventPort_in<bool> processing_ready;
        AADL::eventPort_in<bool> display_done;
        AADL::eventPort_in<bool> display_ready;
      public:
        synchrop(AADL::moduleName name) : AADL::processType(name) {
        }
    };
  }

  namespace video_software_appli {
    typedef My_Capture capturet_DOT_impl;
  }

  namespace video_software_appli {
    class capturep : public AADL::processType {
      public: 
        AADL::eventDataPort_in<video_software_appli::imageidtype> start;
        AADL::eventDataPort_in<video_software_appli::vgapixeltype> pixel;
        AADL::eventPort_out<bool> done;
        AADL::eventPort_out<bool> ready;
      public:
        capturep(AADL::moduleName name) : AADL::processType(name) {
        }
    };
  }

  namespace video_software_appli {
    typedef My_Processing processingt_DOT_impl;
  }

  namespace video_software_appli {
    class processingp : public AADL::processType {
      public: 
        AADL::eventDataPort_in<video_software_appli::imageidtype> start;
        AADL::eventPort_out<bool> done;
        AADL::eventPort_out<bool> ready;
      public:
        processingp(AADL::moduleName name) : AADL::processType(name) {
        }
    };
  }

  namespace video_software_appli {
    typedef My_Display displayt_DOT_impl;
  }

  namespace video_software_appli {
    class displayp : public AADL::processType {
      public: 
        AADL::eventDataPort_in<video_software_appli::imageidtype> start;
        AADL::eventDataPort_out<video_software_appli::vgapixeltype> pixel;
        AADL::eventPort_out<bool> done;
        AADL::eventPort_out<bool> ready;
      public:
        displayp(AADL::moduleName name) : AADL::processType(name) {
        }
    };
  }

  namespace video_software_appli {
    class synchrop_DOT_impl : public video_software_appli::synchrop {
      public: 
        video_software_appli::synchrot_DOT_impl synchro0;
      public: 
      public:
        synchrop_DOT_impl(AADL::moduleName name) : synchrop(name), synchro0("synchro0") {
          synchro0.capture_ready(capture_ready);
          synchro0.capture_done(capture_done);
          synchro0.capture_start(capture_start);
          synchro0.processing_ready(processing_ready);
          synchro0.processing_done(processing_done);
          synchro0.processing_start(processing_start);
          synchro0.display_ready(display_ready);
          synchro0.display_done(display_done);
          synchro0.display_start(display_start);
        }
    };
  }

  namespace video_software_appli {
    class capturep_DOT_impl : public video_software_appli::capturep {
      public: 
        video_software_appli::capturet_DOT_impl capture0;
      public: 
      public:
        capturep_DOT_impl(AADL::moduleName name) : capturep(name), capture0("capture0") {
          capture0.start(start);
          capture0.done(done);
          capture0.ready(ready);
          capture0.pixel(pixel);
        }
    };
  }

  namespace video_software_appli {
    class processingp_DOT_impl : public video_software_appli::processingp {
      public: 
        video_software_appli::processingt_DOT_impl processing0;
      public: 
      public:
        processingp_DOT_impl(AADL::moduleName name) : processingp(name), processing0("processing0") {
          processing0.start(start);
          processing0.done(done);
          processing0.ready(ready);
        }
    };
  }

  namespace video_software_appli {
    class displayp_DOT_impl : public video_software_appli::displayp {
      public: 
        video_software_appli::displayt_DOT_impl display0;
      public: 
      public:
        displayp_DOT_impl(AADL::moduleName name) : displayp(name), display0("display0") {
          display0.start(start);
          display0.done(done);
          display0.ready(ready);
          display0.pixel(pixel);
        }
    };
  }

  namespace video_software_appli {
    class severalprocesses : public AADL::systemType {
      public: 
        AADL::eventDataPort_in<video_software_appli::vgapixeltype> pixel_in;
        AADL::eventDataPort_out<video_software_appli::vgapixeltype> pixel_out;
      public:
        severalprocesses(AADL::moduleName name) : AADL::systemType(name) {
        }
    };
  }

  namespace video_software_appli {
    class hw : public AADL::systemType {
      public:
        hw(AADL::moduleName name) : AADL::systemType(name) {
        }
    };
  }

  namespace video_software_appli {
    class singleprocess : public AADL::processType {
      public: 
        AADL::eventDataPort_in<video_software_appli::vgapixeltype> pixel_in;
        AADL::eventDataPort_out<video_software_appli::vgapixeltype> pixel_out;
      public:
        singleprocess(AADL::moduleName name) : AADL::processType(name) {
        }
    };
  }

  namespace video_software_appli {
    class severalprocesses_DOT_impl : public video_software_appli::severalprocesses {
      public: 
        video_software_appli::processingp_DOT_impl processing0;
        video_software_appli::synchrop_DOT_impl synchro0;
        video_software_appli::capturep_DOT_impl capture0;
        video_software_appli::displayp_DOT_impl display0;
      public: 
        AADL::channel<video_software_appli::imageidtype> c_capture_start;
        AADL::channel<bool> c_capture_ready;
        AADL::channel<bool> c_capture_done;
        AADL::channel<video_software_appli::imageidtype> c_processing_start;
        AADL::channel<bool> c_processing_ready;
        AADL::channel<bool> c_processing_done;
        AADL::channel<video_software_appli::imageidtype> c_display_start;
        AADL::channel<bool> c_display_ready;
        AADL::channel<bool> c_display_done;
      public:
        severalprocesses_DOT_impl(AADL::moduleName name) : severalprocesses(name), processing0("processing0"), synchro0("synchro0"), capture0("capture0"), display0("display0") {
          capture0.pixel(pixel_in);
          synchro0.capture_start(c_capture_start);
          capture0.start(c_capture_start);
          capture0.ready(c_capture_ready);
          synchro0.capture_ready(c_capture_ready);
          capture0.done(c_capture_done);
          synchro0.capture_done(c_capture_done);
          synchro0.processing_start(c_processing_start);
          processing0.start(c_processing_start);
          processing0.ready(c_processing_ready);
          synchro0.processing_ready(c_processing_ready);
          processing0.done(c_processing_done);
          synchro0.processing_done(c_processing_done);
          synchro0.display_start(c_display_start);
          display0.start(c_display_start);
          display0.ready(c_display_ready);
          synchro0.display_ready(c_display_ready);
          display0.done(c_display_done);
          synchro0.display_done(c_display_done);
          display0.pixel(pixel_out);
        }
    };
  }

  namespace video_software_appli {
    class hw_DOT_impl : public video_software_appli::hw {
      public:
        hw_DOT_impl(AADL::moduleName name) : hw(name) {
        }
    };
  }

  namespace video_software_appli {
    class s : public AADL::systemType {
      public: 
        AADL::eventDataPort_in<video_software_appli::vgapixeltype> pixel_in;
        AADL::eventDataPort_out<video_software_appli::vgapixeltype> pixel_out;
      public:
        s(AADL::moduleName name) : AADL::systemType(name) {
        }
    };
  }

  namespace video_software_appli {
    typedef ImageArray imagearraytype;
  }

  namespace video_software_appli {
    class singleprocess_DOT_impl : public video_software_appli::singleprocess {
      public: 
        video_software_appli::processingt_DOT_impl processing0;
        video_software_appli::synchrot_DOT_impl synchro0;
        video_software_appli::capturet_DOT_impl capture0;
        video_software_appli::displayt_DOT_impl display0;
      public: 
        AADL::channel<video_software_appli::imageidtype> c_capture_start;
        AADL::channel<bool> c_capture_ready;
        AADL::channel<bool> c_capture_done;
        AADL::channel<video_software_appli::imageidtype> c_processing_start;
        AADL::channel<bool> c_processing_ready;
        AADL::channel<bool> c_processing_done;
        AADL::channel<video_software_appli::imageidtype> c_display_start;
        AADL::channel<bool> c_display_ready;
        AADL::channel<bool> c_display_done;
      public:
        singleprocess_DOT_impl(AADL::moduleName name) : singleprocess(name), processing0("processing0"), synchro0("synchro0"), capture0("capture0"), display0("display0") {
          capture0.pixel(pixel_in);
          synchro0.capture_start(c_capture_start);
          capture0.start(c_capture_start);
          capture0.ready(c_capture_ready);
          synchro0.capture_ready(c_capture_ready);
          capture0.done(c_capture_done);
          synchro0.capture_done(c_capture_done);
          synchro0.processing_start(c_processing_start);
          processing0.start(c_processing_start);
          processing0.ready(c_processing_ready);
          synchro0.processing_ready(c_processing_ready);
          processing0.done(c_processing_done);
          synchro0.processing_done(c_processing_done);
          synchro0.display_start(c_display_start);
          display0.start(c_display_start);
          display0.ready(c_display_ready);
          synchro0.display_ready(c_display_ready);
          display0.done(c_display_done);
          synchro0.display_done(c_display_done);
          display0.pixel(pixel_out);
        }
    };
  }

  namespace video_software_appli {
    class signal : public AADL::busType {
      public:
        signal(AADL::moduleName name) : AADL::busType(name) {
        }
    };
  }

  namespace video_software_appli {
    class vgapixelbus : public AADL::busType {
      public:
        vgapixelbus(AADL::moduleName name) : AADL::busType(name) {
        }
    };
  }

  namespace video_software_appli {
    class imageidbus : public AADL::busType {
      public:
        imageidbus(AADL::moduleName name) : AADL::busType(name) {
        }
    };
  }

  namespace video_software_appli {
    class systembus : public AADL::busType {
      public:
        systembus(AADL::moduleName name) : AADL::busType(name) {
        }
    };
  }

  namespace video_software_appli {
    class s_DOT_impl : public video_software_appli::s {
      public: 
        video_software_appli::severalprocesses_DOT_impl sw0;
        video_software_appli::hw_DOT_impl hw0;
      public: 
      public:
        s_DOT_impl(AADL::moduleName name) : s(name), sw0("sw0"), hw0("hw0") {
          sw0.pixel_in(pixel_in);
          sw0.pixel_out(pixel_out);
        }
    };
  }
} // end of namespace AADL_video_software_appli
