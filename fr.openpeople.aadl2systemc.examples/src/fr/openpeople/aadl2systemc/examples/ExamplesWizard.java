/*******************************************************************************
 * Copyright (c) 2011 Lab-STICC Universite de Bretagne Sud, Lorient.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the CeCILL-B license available
 * at :
 * en : http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html
 * fr : http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * 
 * Contributors:
 * Dominique BLOUIN (Lab-STICC UBS), dominique.blouin@univ-ubs.fr
 ******************************************************************************/
package fr.openpeople.aadl2systemc.examples;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Platform;

import fr.tpt.mem4csd.utils.eclipse.ui.AbstractExampleWizard;

/**
 * Create the AADL to SystemC example projects.
 * @author <a href="mailto:dominique.blouin@univ-ubs.fr">Dominique BLOUIN</a>
 */
public class ExamplesWizard extends AbstractExampleWizard {
	
	public ExamplesWizard() {
	}

	@Override
	protected String[] getProjectNames() {
		final List<String>projects = new ArrayList<String>();
		projects.add( "ex_Video_Processing_SW_App_AADL" );

		if ( Platform.getBundle( "org.eclipse.cdt.ui" ) != null ) {
		    final String osName = System.getProperty( "os.name" );

		    if ( osName != null ) {
		    	if ( osName.toLowerCase().contains( "linux" ) ) {
		    	}
		    	else if ( osName.toLowerCase().contains( "win" ) ) {
		    		projects.add( "ex_Video_Processing_SW_App_systemc_win" );
		    	}
		    }
		}
		
		return projects.toArray( new String[ projects.size() ] );
	}

	@Override
	protected String getPluginId() {
		return Activator.PLUGIN_ID;
	}
	
	@Override
	protected String getExamplesSourceDir() {
		return "examples_src";
	}
}
