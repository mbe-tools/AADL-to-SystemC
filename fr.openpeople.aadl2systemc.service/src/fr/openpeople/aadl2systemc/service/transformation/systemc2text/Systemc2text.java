package fr.openpeople.aadl2systemc.service.transformation.systemc2text;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.m2m.atl.common.ATLExecutionException;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.IModel;
import org.eclipse.m2m.atl.engine.asm.ASM;

import fr.labsticc.filesystem.model.filesystem.File;
import fr.labsticc.filesystem.model.filesystem.FilesystemFactory;
import fr.openpeople.aadl2systemc.service.transformation.AbstractTransformation;

/**
 * Entry point of the 'Systemc2text' transformation module.
 */
public class Systemc2text extends AbstractTransformation {
	
	// DO not remove this line. It use to force a dependency on a plugin needed so that the writeTis performed by 
	// org.eclipse.m2m.atl.engine.asm.lib.ASMString in this plugin.
	@SuppressWarnings("unused")
	private static final Class<?> clazz = ASM.class;
	
	public Systemc2text()
	throws IOException {
		super( "Systemc2text" );
	}

	@Override
	public List<IModel> doTransformation( 	final List<IModel> p_inModels,
											final String p_outModelsPath,
											final IProgressMonitor p_progrMonitor )
	throws ATLCoreException, IOException, ATLExecutionException {
		
		// Adds a model for the file since this is a Query
		// (no out model for this transformation so pass a second model to specify the out file).
		final File outCppFile = FilesystemFactory.eINSTANCE.createFile();
		outCppFile.setId( p_outModelsPath );
		final IModel fileModel = createModel( inputMmAliases.get( inputMmAliases.size() - 1 ) );
		final ResourceSet resSet = new ResourceSetImpl();
		final Resource res = resSet.createResource( URI.createURI( "" ) );
		res.getContents().add( outCppFile );
		final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		res.save( outStream, null );
		final ByteArrayInputStream inStream = new ByteArrayInputStream( outStream.toByteArray() );
		injector.inject( fileModel, inStream, getOptions() );
		p_inModels.add( fileModel );

		return super.doTransformation( p_inModels, null, p_progrMonitor );
	}
}
