package fr.openpeople.aadl2systemc.service.transformation.updaterefs;

import java.io.IOException;

import fr.openpeople.aadl2systemc.service.transformation.AbstractTransformation;

/**
 * Entry point of the 'UpdateRefs' transformation module.
 */
public class UpdateRefs extends AbstractTransformation {

	/**
	 * Constructor.
	 *
	 * @generated
	 */
	public UpdateRefs()
	throws IOException {
		super( "UpdateRefs" );
	}
}
