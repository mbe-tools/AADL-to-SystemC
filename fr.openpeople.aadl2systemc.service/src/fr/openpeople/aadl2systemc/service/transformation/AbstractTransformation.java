package fr.openpeople.aadl2systemc.service.transformation;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EPackageRegistryImpl;
import org.eclipse.m2m.atl.common.ATLExecutionException;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.IExtractor;
import org.eclipse.m2m.atl.core.IInjector;
import org.eclipse.m2m.atl.core.IModel;
import org.eclipse.m2m.atl.core.IReferenceModel;
import org.eclipse.m2m.atl.core.ModelFactory;
import org.eclipse.m2m.atl.core.emf.EMFExtractor;
import org.eclipse.m2m.atl.core.emf.EMFInjector;
import org.eclipse.m2m.atl.core.emf.EMFModelFactory;
import org.eclipse.m2m.atl.core.launch.ILauncher;
import org.eclipse.m2m.atl.engine.emfvm.launch.EMFVMLauncher;

public abstract class AbstractTransformation {
	
	private static final String PROPERTIES_EXT = ".properties";
	
	protected static final String EXT_SEPARATOR = ".";
	private static final String LIST_SEPARATOR = ",";
	private static final String LIB_ALIAS_PREFIX = "LIB";

	/**
	 * The property file. Stores module list, the meta-model and library locations.
	 */
	private final Properties properties;

	private final String moduleName;
	
	protected final List<String> inputModelsAliases;
	protected final List<String> inputMmAliases;
	
	protected final List<String> outputModelsAliases;
	protected final List<String> outputMmAliases;
	
	protected final ModelFactory modelFactory;
	
	protected final IInjector injector;
	
	protected final IExtractor extractor;

	protected AbstractTransformation( final String p_moduleName ) 
	throws IOException {
		moduleName = p_moduleName;

		properties = new Properties();
		properties.load( getFileURL( moduleName + PROPERTIES_EXT ).openStream() );
		
		inputMmAliases = parseList( moduleName + ".metamodels.input" );
		inputModelsAliases = parseList( moduleName + ".models.input" );
		outputMmAliases = parseList( moduleName + ".metamodels.output" );
		outputModelsAliases = parseList( moduleName + ".models.output" );
		
		modelFactory = createModelFactory();
		injector = new EMFInjector();
		extractor = new EMFExtractor();
	}
	
	protected List<String> parseList( final String p_key ) {
		final String list = properties.getProperty( p_key );
		
		if ( list == null ) {
			return Collections.emptyList();
		}

		final List<String> elements = new ArrayList<String>();
		
		for ( final String element : list.split( LIST_SEPARATOR ) ) {
			elements.add( element.trim() );
		}
		
		return elements;
	}
	
	protected List<IModel> loadInputModels( final List<String> p_inModelsPaths )
	throws ATLCoreException {
		validateModelPaths( p_inModelsPaths, inputMmAliases, "input" );
		
		final List<IModel> models = new ArrayList<IModel>();
		int index = 0;
		
		for ( final String inputMmAlias : inputMmAliases ) {
			models.add( loadModel( p_inModelsPaths.get( index++ ), inputMmAlias ) );
		}
		
		return models;
	}
	
	protected List<IModel> createOutputModels() 
	throws ATLCoreException {
		final List<IModel> models = new ArrayList<IModel>();

		for ( final String outputMmAlias : outputMmAliases ) {
			final IReferenceModel outputMetamodel = modelFactory.newReferenceModel();
			injector.inject( outputMetamodel, getMetamodelUri( outputMmAlias ) );
			models.add( modelFactory.newModel( outputMetamodel ) );
		}
		
		return models;
	}
	
	protected IModel loadModel( final String p_modelPath,
								final String p_mmAlias ) 
	throws ATLCoreException {
		final IModel model = createModel( p_mmAlias );
		injector.inject( model, p_modelPath );

		return model;
	}
	
	protected IModel createModel( final String p_mmAlias ) 
	throws ATLCoreException {
		final IReferenceModel metamodel = modelFactory.newReferenceModel();
		injector.inject( metamodel, getMetamodelUri( p_mmAlias ) );

		return modelFactory.newModel( metamodel );
	}
	
	private static void validateModelPaths( 	final Collection<String> p_modelsPaths,
												final Collection<String> p_mmAliases,
												final String p_type ) {
		if ( p_modelsPaths.size() < p_mmAliases.size() ) {
			throw new IllegalArgumentException( "Not enough " + p_type + " model paths (" + p_modelsPaths.size() + " instead of " + p_mmAliases.size() +"." );
		}

		if ( p_modelsPaths.size() > p_mmAliases.size() ) {
			throw new IllegalArgumentException( "Too many " + p_type + " model paths (" + p_modelsPaths.size() + " instead of " + p_mmAliases.size() +"." );
		}
	}
	
	protected ModelFactory createModelFactory() {
		return new EMFModelFactory();
	}

	protected void saveModels( 	final String p_outModelsPath,
								final Collection<IModel> p_models )
	throws ATLCoreException {
		int index = 0;
		
		for ( final IModel model : p_models ) {
			extractor.extract( model, p_outModelsPath + "/" + moduleName + "_" + outputModelsAliases.get( index ) + EXT_SEPARATOR + getFileExtension( outputMmAliases.get( index ) ) );
			index++;
		}
	}
	
	protected URL getFileURL( final String p_fileName )
	throws IOException {
		final URL fileURL;
		
		if (isEclipsePlatformRunning()) {
			URL resourceURL = getClass().getResource( p_fileName );
			
			if ( resourceURL != null ) {
				fileURL = FileLocator.toFileURL(resourceURL);
			}
			else {
				fileURL = null;
			}
		}
		else {
			fileURL = getClass().getResource( p_fileName );
		}
		
		if ( fileURL == null ) {
			throw new IOException( "'" + p_fileName + "' not found" );
		}
		
		return fileURL;
	}

	protected static boolean isEclipsePlatformRunning() {
		try {
			return Platform.isRunning();
		} catch (Throwable exception) {
			// Assume that we aren't running.
		}
		return false;
	}
	
	protected String getMetamodelUri( final String p_metamodelName ) {
		return properties.getProperty( moduleName + ".metamodels." + p_metamodelName );
	}
	
	protected String getFileExtension( final String p_metaModelAlias ) {
		final EPackage ePackage = EPackageRegistryImpl.INSTANCE.getEPackage( getMetamodelUri( p_metaModelAlias ) );
		
		return ePackage == null ? "xmi" : ePackage.getName() == null ? "xmi" : ePackage.getName();
	}

	protected String getFileSuffix() {
		return "_" + moduleName;
	}

	public List<IModel> doTransformationFromResources( 	final List<String> p_inModelPaths,
														final String p_outModelsPath,
														final IProgressMonitor p_monitor )
	throws ATLCoreException, IOException, ATLExecutionException {
		return doTransformation( loadInputModels( p_inModelPaths ), p_outModelsPath, p_monitor );
	}
	
	public List<IModel> doTransformation( 	final List<IModel> p_inModels,
											final String p_outModelsPath,
											final IProgressMonitor p_monitor )
	throws ATLCoreException, IOException, ATLExecutionException {
		final ILauncher launcher = createLauncher();
		final Map<String, Object> launcherOptions = getOptions();
		launcher.initialize( launcherOptions );
		
		int index = 0;
		
		for ( final IModel model : p_inModels ) {
			launcher.addInModel( model, inputModelsAliases.get( index ), inputMmAliases.get( index ) );
			index++;
		}
		
		index = 0;
		final List<IModel> outModels =  createOutputModels();
		
		for ( final IModel model : outModels ) {
			launcher.addOutModel( model, outputModelsAliases.get( index ), outputMmAliases.get( index ) );
			index++;
		}
		
		index = 0;
		
		for ( final InputStream library : getLibrariesAsStream() ) {
			launcher.addLibrary( LIB_ALIAS_PREFIX + index++, library );
		}
		
		launcher.launch( "run", p_monitor, launcherOptions, (Object[]) getModulesList() );
		
		if ( p_outModelsPath != null ) {
			saveModels( p_outModelsPath, outModels );
		}
		
		return outModels;
	}
	
	protected ILauncher createLauncher() {
		return new EMFVMLauncher();
	}
	
	protected Map<String, Object> getOptions() {
		final Map<String, Object> options = new HashMap<String, Object>();
		final String optionsName = moduleName + ".options.";
		
		for (Entry<Object, Object> entry : properties.entrySet()) {
			if (entry.getKey().toString().startsWith( optionsName )) {
				options.put(entry.getKey().toString().replaceFirst( optionsName, "" ), 
				entry.getValue().toString());
			}
		}
		
		return options;
	}

	protected InputStream[] getModulesList() throws IOException {
		final Collection<InputStream> modules = new ArrayList<InputStream>();

		for ( final String module : parseList( moduleName + ".modules" ) ) {
			final String asmModulePath = new Path( module ).removeFileExtension().addFileExtension( "asm" ).toString();
			modules.add( getFileURL(asmModulePath).openStream() );
		}
		
		return modules.toArray( new InputStream[ modules.size() ] );
	}

	protected Collection<InputStream> getLibrariesAsStream()
	throws IOException {
		final Collection<InputStream> libraryStreams = new HashSet<InputStream>();

		for ( final String libName : parseList( moduleName + ".libraries" ) ) {
			libraryStreams.add( getFileURL( libName.trim() ).openStream() );
		}
		
		return libraryStreams;
	}
	
	@Override
	public String toString() {
		return moduleName;
	}
}
