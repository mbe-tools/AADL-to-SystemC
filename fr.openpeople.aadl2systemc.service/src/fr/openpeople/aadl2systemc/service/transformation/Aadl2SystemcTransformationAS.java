package fr.openpeople.aadl2systemc.service.transformation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.IModel;

import fr.openpeople.aadl2systemc.service.transformation.aadl2systemc.Aadl2systemc;
import fr.openpeople.aadl2systemc.service.transformation.orderclasses.OrderClasses;
import fr.openpeople.aadl2systemc.service.transformation.systemc2text.Systemc2text;
import fr.openpeople.aadl2systemc.service.transformation.updaterefs.UpdateRefs;
import fr.openpeople.aadl2systemc.service.transformation.updaterefs2.UpdateRefs2;

public class Aadl2SystemcTransformationAS {
	
	private static final Logger LOGGER = Logger.getLogger( Aadl2SystemcTransformationAS.class.getName() );
	
	private final List<AbstractTransformation> transformations;
	
	public Aadl2SystemcTransformationAS() 
	throws IOException {
		transformations = new ArrayList<AbstractTransformation>();
		transformations.add( new Aadl2systemc() );
		transformations.add( new UpdateRefs() );
		transformations.add( new UpdateRefs2() );
		transformations.add( new OrderClasses() );
		transformations.add( new Systemc2text() );
	}
	
	public int numberTicks( final Collection<IFile> p_filesToTransform ) {
		return p_filesToTransform.size() * transformations.size();
	}
	
	public void doTransformation( 	final Collection<IFile> p_filesToTransform,
									final String p_outDirectory,
									final IProgressMonitor p_monitor ) 
	throws IOException, ATLCoreException {
		for ( final IFile file : p_filesToTransform ) {
			if ( p_monitor.isCanceled() ) {
				return;
			}
			
			String inputFile = "platform:/resource" + file.getFullPath().toString();
			final String outModelFile = p_outDirectory + File.separator + file.getName().replace( "." + file.getFileExtension(), "" );				
			final Iterator<AbstractTransformation> transIt = transformations.iterator();
			AbstractTransformation transformation = transIt.next();
			LOGGER.info( "Performing " + transformation.toString() + " transformation" );
			List<IModel> outModels = transformation.doTransformationFromResources( 	Collections.singletonList( inputFile ), 
																					null,
																					SubMonitor.convert( p_monitor, 1 ) );
			
			if ( LOGGER.isLoggable( Level.FINEST ) ) {
				//transformation.saveModels( p_outDirectory, outModels );
			}

			p_monitor.worked( 1 );
			
			while ( transIt.hasNext() ) {
				transformation = transIt.next();
				
				if ( transIt.hasNext() ) {
					outModels = transformation.doTransformation( outModels, null, SubMonitor.convert( p_monitor, 1 ) );
				}
				else {
					// Pass the out file to write the result for the last transformation
					outModels = transformation.doTransformation( outModels, outModelFile, p_monitor );
				}

				if ( LOGGER.isLoggable( Level.FINEST ) ) {
					//transformation.saveModels( p_outDirectory, outModels );
				}

				p_monitor.worked( 1 );
			}
		}
	}
}
