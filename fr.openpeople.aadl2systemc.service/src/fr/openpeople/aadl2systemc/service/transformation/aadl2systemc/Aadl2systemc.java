package fr.openpeople.aadl2systemc.service.transformation.aadl2systemc;

import java.io.IOException;

import org.eclipse.m2m.atl.core.ModelFactory;
import org.eclipse.m2m.atl.core.emf.EMFModelFactory;
import org.eclipse.m2m.atl.core.launch.ILauncher;
import org.eclipse.m2m.atl.engine.emfvm.launch.EMFVMLauncher;

//import fr.labsticc.framework.atl.osate.mh.emfvm.OSATEEMFModelFactory;
//import fr.labsticc.framework.atl.osate.mh.emfvm.OSATEEMFVMLauncher;
import fr.openpeople.aadl2systemc.service.transformation.AbstractTransformation;

/**
 * Entry point of the 'Aadl2systemc' transformation module.
 */
public class Aadl2systemc extends AbstractTransformation {

	/**
	 * Constructor.
	 *
	 * @generated
	 */
	public Aadl2systemc() 
	throws IOException {
		super( "Aadl2systemc" );
	}
	
	@Override
	protected ILauncher createLauncher() {
		return new EMFVMLauncher();
		//return new OSATEEMFVMLauncher();
	}
	
	@Override
	protected ModelFactory createModelFactory() {
		return new EMFModelFactory();
		//return new OSATEEMFModelFactory();
	}
}
