package fr.openpeople.aadl2systemc.service.transformation.orderclasses;

import java.io.IOException;

import fr.openpeople.aadl2systemc.service.transformation.AbstractTransformation;

/**
 * Entry point of the 'OrderClasses' transformation module.
 */
public class OrderClasses extends AbstractTransformation {
		
	public OrderClasses()
	throws IOException {
		super( "OrderClasses" );
	}
}
