/*******************************************************************************
 * Copyright (c) 2010 Obeo.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package fr.openpeople.aadl2systemc.service.transformation.updaterefs2;

import java.io.IOException;

import fr.openpeople.aadl2systemc.service.transformation.AbstractTransformation;

/**
 * Entry point of the 'UpdateRefs2' transformation module.
 */
public class UpdateRefs2 extends AbstractTransformation {

	/**
	 * Constructor.
	 *
	 * @generated
	 */
	public UpdateRefs2()
	throws IOException {
		super( "UpdateRefs2" );
	}
}
