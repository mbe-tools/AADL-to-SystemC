package fr.openpeople.aadl2systemc.ide.handlers;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.logging.Logger;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.OperationCanceledException;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.ui.dialogs.WorkspaceResourceDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.handlers.HandlerUtil;
import org.osate.aadl2.modelsupport.FileNameConstants;

import fr.openpeople.aadl2systemc.service.transformation.Aadl2SystemcTransformationAS;

/**
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class GenerateSystemCHandler extends AbstractHandler {
	
	private static final Logger LOGGER = Logger.getLogger(GenerateSystemCHandler.class.getName() );

	private Aadl2SystemcTransformationAS transformationDelegate;
	
	/**
	 * The constructor.
	 */
	public GenerateSystemCHandler() {
		try {
			transformationDelegate = new Aadl2SystemcTransformationAS();
			LOGGER.info( "AADL to SystemC hanlder instantiated." );
		}
		catch ( final IOException ex ) {
			LOGGER.severe( ex.toString() );
			
			transformationDelegate = null;
		}
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute( final ExecutionEvent event )
	throws ExecutionException {
		final Collection<IFile> filesToTransform = aadlResources( event );
		final Shell shell = HandlerUtil.getActiveShell( event );
		final IContainer[] dirs = WorkspaceResourceDialog.openFolderSelection( 	shell,
																				"Workspace Directory Selection", 
																				"Select directory to contain output files...", 
																				false, 
																				null, 
																				null );

		if ( dirs != null && dirs.length > 0 ) {
			final WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {

				// This is the method that gets invoked when the operation runs.
				@Override
				public void execute( final IProgressMonitor monitor ) {
					String message = "Generating SystemC File";
					
					if ( filesToTransform.size() > 1 ) {
						message += "s";
					}
					
					monitor.beginTask( message, transformationDelegate.numberTicks( filesToTransform ) + 1 );
					
					try {
						transformationDelegate.doTransformation( 	filesToTransform,
																	dirs[ 0 ].getFullPath().toString(),
																	monitor );
						dirs[ 0 ].refreshLocal( IContainer.DEPTH_ONE, SubMonitor.convert( monitor, 1 ));//new SubProgressMonitor( monitor, 1 ) );
						monitor.worked( 1 );
					}
					catch ( final OperationCanceledException ex ) {
					}
					catch ( final Throwable th ) {
						handleException( shell,	"Unable to transform AADL models to System C", th );
					}
					finally {
						monitor.done();
					}
				}
			};

			try {
				new ProgressMonitorDialog( shell ).run( true, true, operation );
			}
			catch ( final InterruptedException ex ) {
				MessageDialog.openInformation( shell, "Interruption", "Generation was interrupted" );
			}
			catch ( final InvocationTargetException ex ) {
				handleException( shell, "Unable to transform AADL models to System C", ex );
			}
		}
		
		return null;
	}
	
	private void handleException( 	final Shell shell,
									final String message,
									final Throwable th ) {
		LOGGER.severe( th.toString() );
		
		final Shell actShell;
		
		if ( shell == null ) {
			actShell = searchShell();
		}
		else {
			actShell = shell;
		}
		
		if ( actShell != null ) {
			Display.getDefault().syncExec( new Runnable() {
				
				@Override
				public void run() {
					MessageDialog.openError( 	actShell,
												"SystemC Generation Error", 
												message + " : " + th );
				}
			} );
		}
	}
	
	private Shell searchShell() {
		final IWorkbenchWindow activeWIndow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		
		if ( activeWIndow != null ) {
			return activeWIndow.getShell();
		}
		
		return PlatformUI.getWorkbench().getDisplay().getActiveShell();
	}
	
	private Collection<IFile> aadlResources( final ExecutionEvent event ) {
		final Collection<IFile> selectedRes = new LinkedHashSet<IFile>();
		final ISelection selection = HandlerUtil.getCurrentSelection( event );
		
		if ( selection instanceof ITreeSelection ) {
			for ( final Object selElement : ( (ITreeSelection) selection ).toList() ) {
				if ( selElement instanceof IFile ) {
					final IFile file = (IFile) selElement;
					
					if ( FileNameConstants.SOURCE_FILE_EXT.equals( file.getFileExtension() ) ) {// IResourceUtility.isAadlFile( file ) ) {
						selectedRes.add( file );
					}
//					else if ( IResourceUtility.isAadlFile( file ) ) {
//						selectedRes.add( IResourceUtility.getComplementFile( file ) );
//					}
				}
			}
		}
		
		return selectedRes;
	}
	
	@Override
	public boolean isHandled() {
		return super.isHandled() && transformationDelegate != null;
	}
	
	@Override
	public boolean isEnabled() {
		return super.isEnabled() && transformationDelegate != null;
	}
}

