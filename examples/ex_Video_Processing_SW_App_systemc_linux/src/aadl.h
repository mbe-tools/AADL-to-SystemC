#ifndef __AADL_INCLUDE__
#define __AADL_INCLUDE__

//#include <iostream>
//using namespace std;

#include "systemc.h"

namespace AADL {

	// empty class
	class emptyClass {};

	// module name
	typedef sc_module_name moduleName;

	// channel class
	template <class T> class channel : public sc_signal<T> {};

	// event port
	template <class T> class eventPort_in    : public sc_port<sc_signal_in_if<bool> >    {};
	template <class T> class eventPort_out   : public sc_port<sc_signal_out_if<bool> >   {};
	template <class T> class eventPort_inout : public sc_port<sc_signal_inout_if<bool> > {};

	// event data port
	template <class T> class eventDataPort_in    : public sc_port<sc_signal_in_if<T> >    {};
	template <class T> class eventDataPort_out   : public sc_port<sc_signal_out_if<T> >   {};
	template <class T> class eventDataPort_inout : public sc_port<sc_signal_inout_if<T> > {};

	// bus access
	template <class T> class busAccess_required   : public sc_port<sc_signal_inout_if<T> > {};
	
	// SW components
	class dataType {};
	class threadType : public sc_module {
	public:
		threadType(sc_module_name name) : sc_module(name) {}
	};
	class processType : public sc_module {
	public:
		processType(sc_module_name name) : sc_module(name) {}
	};

	// HW components
	class busType : public sc_module {
	public:
		busType(sc_module_name name) : sc_module(name) {}
	};
	class deviceType : public sc_module {
	public:
		deviceType(sc_module_name name) : sc_module(name) {}
	};
	class memoryType : public sc_module {
	public:
		memoryType(sc_module_name name) : sc_module(name) {}
	};
	class processorType : public sc_module {
	public:
		processorType(sc_module_name name) : sc_module(name) {}
	};

	// Composite components
	class systemType : public sc_module {
	public:
		systemType(sc_module_name name) : sc_module(name) {}
	};

}

#endif // __AADL_INCLUDE__
