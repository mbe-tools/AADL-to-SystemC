#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include "VGAtypes.h"
#include "video_test.h"
#include <systemc.h>
using namespace std;

SC_MODULE(My_Display) {

    sc_port<sc_signal_out_if<bool> > done;
    sc_port<sc_signal_out_if<bool> > ready;
    sc_port<sc_signal_in_if<ImageId> > start;
    sc_port<sc_signal_out_if<VGAPixel> > pixel;

	SC_CTOR(My_Display) {SC_THREAD(My_Display_code);}

	void My_Display_code() {
		done->write(false);
		//cout << "Display: " << "signaling ready" << endl;
		ready->write(true);
		for(;;){
			sc_time period(int(PIXEL_PERIOD*1000000000), SC_NS);
			bool eoi;
			// wait for start signal
			//cout << "Display: " << "waiting for start" << endl;
			wait(start->default_event());
			ImageId id = start->read();
			Pixel *image_base = id.pixelAddr();
			//cout << "Display: " << "base=" << hex << image_base << dec << endl;
			done->write(false);
			// process image
			eoi = false; while(!eoi) {
				VGAPixel vgap = display_pixel(image_base, eoi);
				//cout << "My_Display_code: " << vgap << endl;
				wait(period);
				pixel->write(vgap);
			}
			// signal processing is done
			//cout << "Display: " << "signaling done" << endl;
			done->write(true);
		}
	}

	VGAPixel display_pixel(Pixel* &pixel_addr, bool &eoi)
	{
		enum STATE {GEN_VSYNC, GEN_1RST_HSYNC, GEN_TOP_LINES, GEN_LINES, GEN_BOTTOM_LINES};
		static STATE state = GEN_VSYNC; // init state of state machine
		static int generated_lines = 0;
		static int generated_pixels = 0;
		//static int col = 0, active_col;
		VGAPixel pout;
		static int valid_pixel = false;
		bool hs;
		Pixel p;

		valid_pixel = ! valid_pixel;
		if (!valid_pixel)
			pout = VGAPixel(false, 0x0, 0x0, 0x0, true, false); // no data
		else switch (state) {
		case GEN_VSYNC: // generate frame synchro
			pout = VGAPixel(true, 0x0, 0x0, 0x0, true, false); // VSYNC
			state = GEN_1RST_HSYNC;
			break;
		case GEN_1RST_HSYNC: // generate 1rst line synchro, immediately after VSYNC
			pout = VGAPixel(true, 0x0, 0x0, 0x0, false, true); // HSYNC
			state = GEN_TOP_LINES;
			generated_lines = 1;
			generated_pixels = 1;
			break;
		case GEN_TOP_LINES: // generate top blank lines
			if (0 == generated_pixels) 
				pout = VGAPixel(true, 0x0, 0x0, 0x0, false, true); // HSYNC
			else                       
				pout = VGAPixel(true, 0x0, 0x0, 0x0, false, false);
			generated_pixels++;
			if (PIXELS_TO_GENERATE == generated_pixels) {
				generated_pixels = 0;
				generated_lines++;
				if (TOP_LINES_TO_GENERATE < generated_lines) {
					state = GEN_LINES;
					generated_lines = 0;
					generated_pixels = 0;
				}
			}
			break;
		case GEN_LINES: // generate active lines	
			if (active_pixel(generated_pixels)) {
				p = *pixel_addr; pixel_addr++;
			} else {
				p = Pixel(0, 0, 0);
			}
			generated_pixels++;
			hs = (1 == generated_pixels) ? true : false;
			pout = VGAPixel(true, p.red, p.green, p.blue, false, hs);
			if (PIXELS_TO_GENERATE == generated_pixels) {
				generated_pixels = 0;
				generated_lines++;
				if (LINES_TO_GENERATE == generated_lines) {
					state = GEN_BOTTOM_LINES;
					generated_lines = 0;
					generated_pixels = 0;
				}
			}
			break;
		case GEN_BOTTOM_LINES: // generate bottom blank lines
			if (0 == generated_pixels) pout = VGAPixel(true, 0x0, 0x0, 0x0, false, true); // HSYNC
			else                       pout = VGAPixel(true, 0x0, 0x0, 0x0, false, false);
			generated_pixels++;
			if (PIXELS_TO_GENERATE == generated_pixels) {
				generated_pixels = 0;
				generated_lines++;
				if (BOTTOM_LINES_TO_GENERATE == generated_lines) {
					state = GEN_VSYNC;
					eoi = true; // signal end of image
					generated_lines = 0;
					generated_pixels = 0;
				}
			}
			break;  
		}
		return pout;
	}

};

#endif // __DISPLAY_H__
