//============================================================================
// Name        : Video_Processing_SW_App_SystemC.cpp
// Author      : Dominique Blouin
// Version     :
// Copyright   :
// Description : Example SystemC application generated from AADL, Ansi-style
//============================================================================
using namespace std;
#include <iostream>
#include <systemc.h>

/**/
// This code is for video processing simulation
#include "video_params.h"
#include "VGAtypes.h"
#include "video_test.h" // for video_generator() and image_generator()
#include "implementation_video.h" // for the system itself
Pixel globalImageArray[PIXELS_IN_ARRAY]; // the global image array

static void main_video(int argc, char* argv[])
{

	sc_signal<AADL_video_processing_sw_app::video_processing_sw_app::vgapixeltype> chanin, chanout;

	// instantiate video generator
	video_generator VIDEO_GEN("VIDEO_GEN");
	VIDEO_GEN.pixel(chanin);
	// instantiate system
	AADL_video_processing_sw_app::video_processing_sw_app::functionalspecification_DOT_impl SYSTEM("SYSTEM");
	SYSTEM.pixel_in(chanin);
	SYSTEM.pixel_out(chanout);

	// instantiate image generator
	image_generator IMAGE_GEN("IMAGE_GEN");
	IMAGE_GEN.pixel(chanout);

	//cout << "globalImageArray=" << hex << globalImageArray << dec << endl;

	// simulate
	sc_time len(int(SIMULATION_LEN*1000), SC_MS);
	cout << "simulation length = " << len << endl;
	sc_start(len);
}
/**/

int sc_main(int argc, char* argv[])
{
	// uncomment the line you want to test
	//main_basic(argc, argv); // basic system simulation
	main_video(argc, argv); // video processing simulation
	return 0;
}
