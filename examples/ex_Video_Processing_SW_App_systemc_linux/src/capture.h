#ifndef __CAPTURE_H__
#define __CAPTURE_H__

#include "VGAtypes.h"
#include <systemc.h>
using namespace std;

SC_MODULE(My_Capture) {

    sc_port<sc_signal_out_if<bool> > done;
    sc_port<sc_signal_out_if<bool> > ready;
    sc_port<sc_signal_in_if<ImageId> > start;
    sc_port<sc_signal_in_if<VGAPixel> > pixel;

	SC_CTOR(My_Capture) {
		SC_THREAD(My_Capture_code); 
	}

	void My_Capture_code() {
		done->write(false);
		//cout << "Capture: " << "signaling ready" << endl;
		ready->write(true);
		for(;;){
			bool eoi = false;
			// wait for start signal
			//cout << "Capture: " << "waiting for start" << endl;
			wait(start->default_event());
			done->write(false);
			ImageId imageAddr = start->read();
			//cout << "Capture: " << "base=" << hex << imageAddr.pixelAddr() << dec << endl;
			// process image
			while(!eoi){
				//cout << "Capture: " << "waiting for pixel" << endl;
				wait(pixel->default_event()); VGAPixel p = pixel->read();
				if (p.valid_data()) eoi = process_pixel(imageAddr, p);
			}
			// signal processing is done
			//cout << "Capture: " << "signaling done" << endl;
			done->write(true);
		}
	}

	inline void store_pixel(Pixel* &pixelAddr, const VGAPixel &p) {
		//cout << "storing pixel: " << p.pixel() << " at @ " << hex << pixelAddr << dec << endl;
		(*pixelAddr) = p.pixel();
		pixelAddr++;
	}

	bool process_pixel(ImageId &imageAddr, const VGAPixel &p)
	{
		enum STATE {WAIT_VSYNC, WAIT_1RST_HSYNC, WAIT_HSYNC, DROP_TOP_LINES, DROP_FRONT_PIXELS, PROCESS_PIXELS, DROP_BACK_PIXELS};
		static STATE state = WAIT_VSYNC; // init state of state machine
		static int dropped_lines, dropped_pixels, processed_lines, processed_pixels;
		static int image_pixel;
		bool eoi = false; // end of im�age detection
		static VGAimage imageOut;
		static Pixel *pixelAddr;

		//cout << "process_pixel: " << p << endl;

			switch (state) {

			case WAIT_VSYNC: // wait frame synchro
				//cout << "process_pixel: " << "WAIT_VSYNC" << endl;
				pixelAddr = imageAddr.pixelAddr();
				//cout << "process_pixel: " << pixelAddr << endl;
				image_pixel = 0; // reset frame buffer pixel position
				dropped_lines = 0;
				processed_lines = 0;
				dropped_pixels = 0;
				processed_pixels = 0;
				if (p.vsync()) { // VSYNC
					if (p.hsync()) { // VSYNC & HSYNC
						state = DROP_TOP_LINES;
					} else { // VSYNC alone
						state = WAIT_1RST_HSYNC;
					}
				}
				break;

			case WAIT_1RST_HSYNC: // wait 1rst line synchro, immediately after VSYNC
				//cout << "process_pixel: " << "WAIT_1RST_HSYNC" << endl;
				if (p.hsync()) { // HSYNC
					state = DROP_TOP_LINES;
				}
				break;

			case DROP_TOP_LINES:
				//cout << "process_pixel: " << "DROP_TOP_LINES" << endl;
				if (p.hsync()) { // HSYNC of next line
					dropped_lines++;
					if (TOP_LINES_TO_DROP == dropped_lines) {
						dropped_pixels = 1; // HSYNC dropped
						processed_lines = 0;
						processed_pixels = 0;
						if (FRONT_PIXELS_TO_DROP == dropped_pixels) {
							state = PROCESS_PIXELS;
						} else {
							state = DROP_FRONT_PIXELS;
						}
					}
				}
				break;

			case WAIT_HSYNC:
				//cout << "process_pixel: " << "WAIT_HSYNC" << endl;
				if (p.hsync()) {
					if (LINES_TO_PROCESS == processed_lines) {
						state = WAIT_VSYNC;
						eoi = true; // signal end of image
					} else {
						dropped_pixels = 1; // HSYNC dropped
						if (FRONT_PIXELS_TO_DROP == dropped_pixels) {
							processed_pixels = 0;
							state = PROCESS_PIXELS;
						} else {
							state = DROP_FRONT_PIXELS;
						}
					}
				}
				break;

			case DROP_FRONT_PIXELS:
				dropped_pixels++;
				//cout << "process_pixel: " << "DROP_FRONT_PIXELS" << endl;
				if (FRONT_PIXELS_TO_DROP == dropped_pixels) {
					state = PROCESS_PIXELS;
					// process 1rst active pixel
					cout << "process_pixel: " << "processing 1rst pixel " << p.pixel() << endl;
					store_pixel(pixelAddr, p);
					processed_pixels = 1;
				}
				break;

			case PROCESS_PIXELS:
				//cout << "process_pixel: " << "PROCESS_PIXELS" << endl;
				store_pixel(pixelAddr, p);
				processed_pixels++;
				if (PIXELS_TO_PROCESS == processed_pixels) {
					processed_lines++;
					state = WAIT_HSYNC;
				}
				break;

			default:
				//cout << "process_pixel: " << "default " << endl;
				//printf("default\n");
				break;

			}

		return eoi;
	}

};

#endif // __CAPTURE_H__
