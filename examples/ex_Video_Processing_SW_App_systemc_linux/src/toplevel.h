//AADL specification(toplevel.aadl) translated into C++(toplevel.h)
#include "aadl.h"
namespace AADL_toplevel {

  namespace video_processing {
    typedef VGAPixel vgapixeltype;
  }

  namespace video_processing {
    typedef ImageId imageidtype;
  }

  namespace video_processing {
    class synchrot : public AADL::threadType {
      public: 
        AADL::eventPort_in<bool> capture_done;
        AADL::eventPort_in<bool> capture_ready;
        AADL::eventPort_in<bool> processing_done;
        AADL::eventPort_in<bool> processing_ready;
        AADL::eventPort_in<bool> display_done;
        AADL::eventPort_in<bool> display_ready;
        AADL::eventDataPort_out<video_processing::imageidtype> capture_start;
        AADL::eventDataPort_out<video_processing::imageidtype> processing_start;
        AADL::eventDataPort_out<video_processing::imageidtype> display_start;
      public:
        synchrot(AADL::moduleName name) : AADL::threadType(name) {
        }
    };
  }

  namespace video_processing {
    class capturet : public AADL::threadType {
      public: 
        AADL::eventPort_out<bool> done;
        AADL::eventPort_out<bool> ready;
        AADL::eventDataPort_in<video_processing::imageidtype> start;
        AADL::eventDataPort_in<video_processing::vgapixeltype> pixel;
      public:
        capturet(AADL::moduleName name) : AADL::threadType(name) {
        }
    };
  }

  namespace video_processing {
    class processingt : public AADL::threadType {
      public: 
        AADL::eventPort_out<bool> done;
        AADL::eventPort_out<bool> ready;
        AADL::eventDataPort_in<video_processing::imageidtype> start;
      public:
        processingt(AADL::moduleName name) : AADL::threadType(name) {
        }
    };
  }

  namespace video_processing {
    class displayt : public AADL::threadType {
      public: 
        AADL::eventPort_out<bool> done;
        AADL::eventPort_out<bool> ready;
        AADL::eventDataPort_in<video_processing::imageidtype> start;
        AADL::eventDataPort_out<video_processing::vgapixeltype> pixel;
      public:
        displayt(AADL::moduleName name) : AADL::threadType(name) {
        }
    };
  }

  namespace video_processing {
    class synchrop : public AADL::processType {
      public: 
        AADL::eventPort_in<bool> capture_done;
        AADL::eventPort_in<bool> capture_ready;
        AADL::eventPort_in<bool> processing_done;
        AADL::eventPort_in<bool> processing_ready;
        AADL::eventPort_in<bool> display_done;
        AADL::eventPort_in<bool> display_ready;
        AADL::eventDataPort_out<video_processing::imageidtype> capture_start;
        AADL::eventDataPort_out<video_processing::imageidtype> processing_start;
        AADL::eventDataPort_out<video_processing::imageidtype> display_start;
      public:
        synchrop(AADL::moduleName name) : AADL::processType(name) {
        }
    };
  }

  namespace video_processing {
    class capturep : public AADL::processType {
      public: 
        AADL::eventPort_out<bool> done;
        AADL::eventPort_out<bool> ready;
        AADL::eventDataPort_in<video_processing::imageidtype> start;
        AADL::eventDataPort_in<video_processing::vgapixeltype> pixel;
      public:
        capturep(AADL::moduleName name) : AADL::processType(name) {
        }
    };
  }

  namespace video_processing {
    class processingp : public AADL::processType {
      public: 
        AADL::eventPort_out<bool> done;
        AADL::eventPort_out<bool> ready;
        AADL::eventDataPort_in<video_processing::imageidtype> start;
      public:
        processingp(AADL::moduleName name) : AADL::processType(name) {
        }
    };
  }

  namespace video_processing {
    class displayp : public AADL::processType {
      public: 
        AADL::eventPort_out<bool> done;
        AADL::eventPort_out<bool> ready;
        AADL::eventDataPort_in<video_processing::imageidtype> start;
        AADL::eventDataPort_out<video_processing::vgapixeltype> pixel;
      public:
        displayp(AADL::moduleName name) : AADL::processType(name) {
        }
    };
  }

  namespace video_processing {
    typedef My_Synchro synchrot_DOT_impl;
  }

  namespace video_processing {
    typedef My_Capture capturet_DOT_impl;
  }

  namespace video_processing {
    typedef My_Processing processingt_DOT_impl;
  }

  namespace video_processing {
    typedef My_Display displayt_DOT_impl;
  }

  namespace video_processing {
    class synchrop_DOT_impl : public video_processing::synchrop {
      public: 
        video_processing::synchrot_DOT_impl synchro0;
      public: 
      public:
        synchrop_DOT_impl(AADL::moduleName name) : synchrop(name), synchro0("synchro0") {
          synchro0.capture_ready(capture_ready);
          synchro0.capture_done(capture_done);
          synchro0.processing_ready(processing_ready);
          synchro0.processing_done(processing_done);
          synchro0.display_ready(display_ready);
          synchro0.display_done(display_done);
          synchro0.capture_start(capture_start);
          synchro0.processing_start(processing_start);
          synchro0.display_start(display_start);
        }
    };
  }

  namespace video_processing {
    class capturep_DOT_impl : public video_processing::capturep {
      public: 
        video_processing::capturet_DOT_impl capture0;
      public: 
      public:
        capturep_DOT_impl(AADL::moduleName name) : capturep(name), capture0("capture0") {
          capture0.done(done);
          capture0.ready(ready);
          capture0.start(start);
          capture0.pixel(pixel);
        }
    };
  }

  namespace video_processing {
    class processingp_DOT_impl : public video_processing::processingp {
      public: 
        video_processing::processingt_DOT_impl processing0;
      public: 
      public:
        processingp_DOT_impl(AADL::moduleName name) : processingp(name), processing0("processing0") {
          processing0.done(done);
          processing0.ready(ready);
          processing0.start(start);
        }
    };
  }

  namespace video_processing {
    class displayp_DOT_impl : public video_processing::displayp {
      public: 
        video_processing::displayt_DOT_impl display0;
      public: 
      public:
        displayp_DOT_impl(AADL::moduleName name) : displayp(name), display0("display0") {
          display0.done(done);
          display0.ready(ready);
          display0.start(start);
          display0.pixel(pixel);
        }
    };
  }

  namespace video_processing {
    class severalprocesses : public AADL::systemType {
      public: 
        AADL::eventDataPort_in<video_processing::vgapixeltype> pixel_in;
        AADL::eventDataPort_out<video_processing::vgapixeltype> pixel_out;
      public:
        severalprocesses(AADL::moduleName name) : AADL::systemType(name) {
        }
    };
  }

  namespace video_processing {
    class hw : public AADL::systemType {
      public:
        hw(AADL::moduleName name) : AADL::systemType(name) {
        }
    };
  }

  namespace video_processing {
    class singleprocess : public AADL::processType {
      public: 
        AADL::eventDataPort_in<video_processing::vgapixeltype> pixel_in;
        AADL::eventDataPort_out<video_processing::vgapixeltype> pixel_out;
      public:
        singleprocess(AADL::moduleName name) : AADL::processType(name) {
        }
    };
  }

  namespace video_processing {
    class s : public AADL::systemType {
      public: 
        AADL::eventDataPort_in<video_processing::vgapixeltype> pixel_in;
        AADL::eventDataPort_out<video_processing::vgapixeltype> pixel_out;
      public:
        s(AADL::moduleName name) : AADL::systemType(name) {
        }
    };
  }

  namespace video_processing {
    class severalprocesses_DOT_impl : public video_processing::severalprocesses {
      public: 
        video_processing::processingp_DOT_impl processing0;
        video_processing::synchrop_DOT_impl synchro0;
        video_processing::capturep_DOT_impl capture0;
        video_processing::displayp_DOT_impl display0;
      public: 
        AADL::channel<bool> c_capture_ready;
        AADL::channel<bool> c_capture_done;
        AADL::channel<bool> c_processing_ready;
        AADL::channel<bool> c_processing_done;
        AADL::channel<bool> c_display_ready;
        AADL::channel<bool> c_display_done;
        AADL::channel<video_processing::imageidtype> c_capture_start;
        AADL::channel<video_processing::imageidtype> c_processing_start;
        AADL::channel<video_processing::imageidtype> c_display_start;
      public:
        severalprocesses_DOT_impl(AADL::moduleName name) : severalprocesses(name), processing0("processing0"), synchro0("synchro0"), capture0("capture0"), display0("display0") {
          capture0.ready(c_capture_ready);
          synchro0.capture_ready(c_capture_ready);
          capture0.done(c_capture_done);
          synchro0.capture_done(c_capture_done);
          processing0.ready(c_processing_ready);
          synchro0.processing_ready(c_processing_ready);
          processing0.done(c_processing_done);
          synchro0.processing_done(c_processing_done);
          display0.ready(c_display_ready);
          synchro0.display_ready(c_display_ready);
          display0.done(c_display_done);
          synchro0.display_done(c_display_done);
          capture0.pixel(pixel_in);
          synchro0.capture_start(c_capture_start);
          capture0.start(c_capture_start);
          synchro0.processing_start(c_processing_start);
          processing0.start(c_processing_start);
          synchro0.display_start(c_display_start);
          display0.start(c_display_start);
          display0.pixel(pixel_out);
        }
    };
  }

  namespace video_processing {
    class hw_DOT_impl : public video_processing::hw {
      public:
        hw_DOT_impl(AADL::moduleName name) : hw(name) {
        }
    };
  }

  namespace video_processing {
    class signal : public AADL::busType {
      public:
        signal(AADL::moduleName name) : AADL::busType(name) {
        }
    };
  }

  namespace video_processing {
    class vgapixelbus : public AADL::busType {
      public:
        vgapixelbus(AADL::moduleName name) : AADL::busType(name) {
        }
    };
  }

  namespace video_processing {
    class imageidbus : public AADL::busType {
      public:
        imageidbus(AADL::moduleName name) : AADL::busType(name) {
        }
    };
  }

  namespace video_processing {
    class systembus : public AADL::busType {
      public:
        systembus(AADL::moduleName name) : AADL::busType(name) {
        }
    };
  }

  namespace video_processing {
    typedef ImageArray imagearraytype;
  }

  namespace video_processing {
    class singleprocess_DOT_impl : public video_processing::singleprocess {
      public: 
        video_processing::processingt_DOT_impl processing0;
        video_processing::synchrot_DOT_impl synchro0;
        video_processing::capturet_DOT_impl capture0;
        video_processing::displayt_DOT_impl display0;
      public: 
        AADL::channel<bool> c_capture_ready;
        AADL::channel<bool> c_capture_done;
        AADL::channel<bool> c_processing_ready;
        AADL::channel<bool> c_processing_done;
        AADL::channel<bool> c_display_ready;
        AADL::channel<bool> c_display_done;
        AADL::channel<video_processing::imageidtype> c_capture_start;
        AADL::channel<video_processing::imageidtype> c_processing_start;
        AADL::channel<video_processing::imageidtype> c_display_start;
      public:
        singleprocess_DOT_impl(AADL::moduleName name) : singleprocess(name), processing0("processing0"), synchro0("synchro0"), capture0("capture0"), display0("display0") {
          capture0.ready(c_capture_ready);
          synchro0.capture_ready(c_capture_ready);
          capture0.done(c_capture_done);
          synchro0.capture_done(c_capture_done);
          processing0.ready(c_processing_ready);
          synchro0.processing_ready(c_processing_ready);
          processing0.done(c_processing_done);
          synchro0.processing_done(c_processing_done);
          display0.ready(c_display_ready);
          synchro0.display_ready(c_display_ready);
          display0.done(c_display_done);
          synchro0.display_done(c_display_done);
          capture0.pixel(pixel_in);
          synchro0.capture_start(c_capture_start);
          capture0.start(c_capture_start);
          synchro0.processing_start(c_processing_start);
          processing0.start(c_processing_start);
          synchro0.display_start(c_display_start);
          display0.start(c_display_start);
          display0.pixel(pixel_out);
        }
    };
  }

  namespace video_processing {
    class s_DOT_impl : public video_processing::s {
      public: 
        video_processing::severalprocesses_DOT_impl sw0;
        video_processing::hw_DOT_impl hw0;
      public: 
      public:
        s_DOT_impl(AADL::moduleName name) : s(name), sw0("sw0"), hw0("hw0") {
          sw0.pixel_in(pixel_in);
          sw0.pixel_out(pixel_out);
        }
    };
  }
} // end of namespace AADL_toplevel
