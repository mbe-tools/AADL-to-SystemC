#ifndef __VGATYPES_H__
#define __VGATYPES_H__

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

class Conv { // all numbers are stored in little endian order
public:
	static unsigned int uint(const char * addr, int n) {
		unsigned int i = 0;
		const char *end = addr + n;
		for(int j = 0; j < n; j++) {
			i = i << 8;
			i += *--end;
		}
		return i;
	}
	static unsigned uint16(const char * addr){return uint(addr, 2);}
	static unsigned uint32(const char * addr){return uint(addr, 4);}
};
class BitMapFileHeader {
public:
	char signature[2];
	char file_size[4];
	char reserved1[2];
	char reserved2[2];
	char file_offset_to_pixel_array[4];
	void dump() const {
		cout << "signature[0]= " << signature[0] << endl;
		cout << "signature[1]= " << signature[1] << endl;
		cout << "file_size   = " << Conv::uint32(file_size) << endl;
		cout << "file_offset = " << Conv::uint32(file_offset_to_pixel_array) << endl;
	}
};

class DIBHeader {
public:
	char DIB_header_size[4];
	char image_width[4];
	char image_height[4];
	char planes[2];
	char bits_per_pixel[2];
	char compression[4];
	char image_size[4];
	char x_pixels_per_meter[4];
	char y_pixels_per_meter[4];
	char colors_in_color_table[4];
	char important_color_count[4];
	void dump() const {
		cout << "DIB_header_size      = " << Conv::uint32(DIB_header_size) << endl;
		cout << "image_width          = " << Conv::uint32(image_width) << endl;
		cout << "image_height         = " << Conv::uint32(image_height) << endl;
		cout << "planes               = " << Conv::uint16(planes) << endl;
		cout << "bits_per_pixel       = " << Conv::uint16(bits_per_pixel) << endl;
		cout << "compression          = " << Conv::uint32(compression) << endl;
		cout << "image_size           = " << Conv::uint32(image_size) << endl;
		cout << "x_pixels_per_meter   = " << Conv::uint32(x_pixels_per_meter) << endl;
		cout << "y_pixels_per_meter   = " << Conv::uint32(y_pixels_per_meter) << endl;
		cout << "colors_in_color_table= " << Conv::uint32(colors_in_color_table) << endl;
		cout << "important_color_count= " << Conv::uint32(important_color_count) << endl;
	}
};

// VGA pixels = (R, V, B), each color is coded with 8 bits, => a char (a byte)
class Pixel {
public:
	unsigned char red, green, blue;
	Pixel(){red = green = blue = 0;}
	Pixel(char r, char g, char b){red = r; green = g; blue = b;}
	friend ostream & operator<<(ostream & o, const Pixel & x);
	bool operator==(const Pixel & x) const {return (red == x.red) && (green == x.green) && (blue && x.blue);}
};
ostream & operator<<(ostream & o, const Pixel & x) {
	o << hex << "RVB=(" 
		<< "0x" << (int(x.red)&0xff) << "," 
		<< "0x" << (int(x.green)&0xff) << "," 
		<< "0x" << (int(x.blue)&0xff) << ")" 
		<< dec;
	return o;
}

// This is the VGA pixel (pixel + VGA synchro signals) that will be used by the AADL model
class VGAPixel {
private:
	Pixel _pixel;
	bool _data_clk, _hsync, _vsync;
public:
	bool valid_data() const {return _data_clk;}
	bool hsync() const {return _hsync;}
	bool vsync() const {return _vsync;}
	Pixel pixel() const {return _pixel;}
	VGAPixel() {
		_data_clk = _hsync = _vsync = false;
	}
	VGAPixel(bool clk, int r, int g, int b, bool v, bool h) {
		_pixel = Pixel(r, g, b);
		_data_clk = clk; _vsync = v; _hsync = h;
	}
	bool operator==(const VGAPixel & x) const {
		return (x._pixel == _pixel) && (x._data_clk == _data_clk) && (x._hsync == _hsync) && (x._vsync == _vsync);
	}
	VGAPixel & operator=(const VGAPixel & x) {
		_pixel = x._pixel;
		_data_clk = x._data_clk; _hsync = x._hsync; _vsync = x._vsync; 
		return *this;
	}
	friend ostream & operator<<(ostream & o, const VGAPixel & x);
};
ostream & operator<<(ostream & o, const VGAPixel & x) {
	if (x._data_clk) o << x._pixel << ", VSYNC=" << x._vsync << ", HSYNC=" << x._hsync;
	else             o << "data clock low";
	return o;
}

class VGAimage {
private:
	bool _valid;
	BitMapFileHeader _bmfh;
	DIBHeader _dh;
	int _width, _height, _padding;
	vector<Pixel> _pixels;
public:
	VGAimage() {_valid = false;}
	VGAimage(const char *name) { // initialize an image from a file
		_valid = false;

		ifstream ifs;
		ifs.open(name, ifstream::in|ifstream::binary);
		if (!ifs.good()) {cout << "unable to open file " << name << endl; return;}

		//cout << "Reading bmfh = " << sizeof(_bmfh) << " bytes" << endl;
		ifs.read((char*)&_bmfh, sizeof(_bmfh)); 
		if (!ifs.good()) {cout << "unable to read bit map file header" << endl; return;}
		if ((_bmfh.signature[0] != 'B') || (_bmfh.signature[1] != 'M')) {
			cout << "bad signature" << endl;
			return;
		}
		//cout << "file size=" << Conv::uint32(_bmfh.file_size) << endl;

		// cout << "Reading dh = " << sizeof(_dh) << " bytes" << endl;
		ifs.read((char*)&_dh, sizeof(_dh)); 
		if (!ifs.good()) {cout << "unable to read DIB header" << endl; return;}
		// allocate a buffer to store all pixels
		_width = Conv::uint32(_dh.image_width);
		_height = Conv::uint32(_dh.image_height);
		int pixels = _width * _height;
		_pixels.resize(pixels, Pixel(0,0,0));
		// read pixels
		int bits_per_pixel = Conv::uint16(_dh.bits_per_pixel);
		if (24 != bits_per_pixel) {
			cout << "Unable to process format other than 24 bits/pixel" << endl;
			return;
		}
		//cout << "bits_per_pixel=" << bits_per_pixel << endl;
		int bits_per_raw = bits_per_pixel * _width;
		//cout << "bits_per_raw=" << bits_per_raw << endl;
		int bytes_per_raw = (bits_per_raw / 8) + (bits_per_raw % 8 ? 1 : 0);
		//cout << "bytes_per_raw=" << bytes_per_raw << endl;
		_padding = bytes_per_raw % 4;
		_padding = _padding ? (4-_padding) : 0;
		//cout << "padding=" << padding << endl;
		//int bytes_per_raw_padded = bytes_per_raw + _padding;
		//cout << "bytes_per_raw_padded=" << bytes_per_raw_padded << endl;
		for(int raw = _height-1; raw >= 0; raw--) {
			for(int column = 0; column < _width; column++) {
				char blue, green, red;
				if (!ifs.good()) {
					cout << "Error while reading pixels" << endl; 
					return;
				}
				ifs.read(&blue, 1); ifs.read(&green, 1); ifs.read(&red, 1);
				int index = raw*_width + column;
				_pixels[index] = Pixel(red, green, blue);
				//cout << "raw=" << raw << " col=" << column << " pixel=" << _pixels[index] << endl;
			}
			char padding_bytes[4];
			ifs.read(padding_bytes, _padding);
		}

		_valid = true;
		cout << "Image " << name << " loaded" << endl;
		ifs.close();
	}
	void dumpFile(const char *name) const { // build a file from an image
		if (!_valid) {
			cout << "Unable to dump a not valid image" << endl;
			return;
		}
		ofstream ofs;
		ofs.open(name, ofstream::out|ifstream::binary);
		if (!ofs.good()) {cout << "unable to open file " << name << endl; return;}

		//cout << "Writing bmfh = " << sizeof(_bmfh) << " bytes" << endl;
		ofs.write((char*)&_bmfh, sizeof(_bmfh)); 
		if (!ofs.good()) {cout << "unable to write bit map file header" << endl; return;}

		// cout << "Writing dh = " << sizeof(_dh) << " bytes" << endl;
		ofs.write((char*)&_dh, sizeof(_dh)); 
		if (!ofs.good()) {cout << "unable to write DIB header" << endl; return;}
		for(int raw = _height-1; raw >= 0; raw--) {
			for(int column = 0; column < _width; column++) {
				int index = raw*_width + column;
				char blue = _pixels[index].blue;
				char green = _pixels[index].green;
				char red = _pixels[index].red;
				if (!ofs.good()) {
					cout << "Error while reading pixels" << endl; 
					return;
				}
				ofs.write(&blue, 1); ofs.write(&green, 1); ofs.write(&red, 1);
			}
			char padding_bytes[4];
			ofs.write(padding_bytes, _padding);
		}
		ofs.close();
		cout << "Image " << name << " dumped" << endl;
	}
	Pixel pixel(int raw, int col) const {
		//cout << "get pixel, raw=" << raw << ", col=" << col << endl;
		int index = raw*_width + col;
		return _pixels[index];
	} 
	void setPixel(const Pixel & p, int raw, int col) {
		//cout << "set pixel, raw=" << raw << ", col=" << col << endl;
		int index = raw*_width + col;
		_pixels[index] = p;
	}
	void dump() const {
		cout << "size=" << _pixels.size() << endl;
		return;
		if (! _valid) return;
		_bmfh.dump();
		_dh.dump();		
	}
};

// Image ID between blocks.
class ImageId {
private:
	Pixel *_addr;
public:
	ImageId() {_addr = 0;}
	ImageId(Pixel *addr) {_addr = addr;}
	bool operator==(const ImageId & x) const {return (_addr == x._addr);}
	ImageId & operator=(const ImageId & x) {_addr = x._addr; return *this;}
	friend ostream & operator<<(ostream & o, const ImageId & x);
	Pixel *pixelAddr() const {return _addr;}
};
ostream & operator<<(ostream & o, const ImageId & x) {o << x._addr; return o;}

// The type of the global image array, for AADL spec only
class ImageArray {
public:
	ImageArray(){}
	bool operator==(const ImageArray & x) const {return true;}
	ImageArray & operator=(const ImageArray & x) {return *this;}
	friend ostream & operator<<(ostream & o, const ImageArray & x);
};
ostream & operator<<(ostream & o, const ImageArray & x) {return o;}

#endif // __VGATYPES_H__

