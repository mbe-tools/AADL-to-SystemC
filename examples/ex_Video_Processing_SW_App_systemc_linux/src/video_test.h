#ifndef __VIDEO_TEST_H__
#define __VIDEO_TEST_H__

#include "video_params.h"
#include "VGAtypes.h"
#include <systemc.h>
using namespace std;

// This image must be out of "video_generator", because "image_generator"
// needs to make a copy of the header.
static VGAimage imageIn("imageIn.bmp"); // set here the name of the file containing the image to process.

SC_MODULE(video_generator) {
    sc_port<sc_signal_out_if<VGAPixel> > pixel;

	// To say if a pixel is an active one or not
	VGAPixel video_gen_pixel()
	{
		enum STATE {GEN_RESET, GEN_VSYNC, GEN_1RST_HSYNC, GEN_TOP_LINES, GEN_LINES, GEN_BOTTOM_LINES};
		static STATE state = GEN_RESET; // init state of state machine
		static int generated_lines = 0;
		static int generated_pixels = 0;
		static int valid_pixel = false;
		VGAPixel pout;
		bool hs;
		Pixel p;

		valid_pixel = ! valid_pixel;
		if (!valid_pixel)
			pout = VGAPixel(false, 0x0, 0x0, 0x0, true, false); // no data
		else switch (state) {
		case GEN_RESET: // generate some blank pixels first to get out of SystemC start
			//cout << "video_generator: " << "GEN_RESET" << endl;
			pout = VGAPixel(true, 0x0, 0x0, 0x0, false, false);
			generated_pixels++;
			if (2 < generated_pixels) {
				state = GEN_VSYNC;
				generated_pixels = 0;
			}
			break;
		case GEN_VSYNC: // generate frame synchro
			//cout << "video_generator: " << "GEN_VSYNC" << endl;
			pout = VGAPixel(true, 0x0, 0x0, 0x0, true, false); // VSYNC
			state = GEN_1RST_HSYNC;
			break;
		case GEN_1RST_HSYNC: // generate 1rst line synchro, immediately after VSYNC
			//cout << "video_generator: " << "GEN_1RST_HSYNC" << endl;
			pout = VGAPixel(true, 0x0, 0x0, 0x0, false, true); // HSYNC
			state = GEN_TOP_LINES;
			generated_lines = 1;
			generated_pixels = 1;
			break;
		case GEN_TOP_LINES: // generate top blank lines
			//cout << "video_generator: " << "GEN_TOP_LINES" << endl;
			if (0 == generated_pixels) 
				pout = VGAPixel(true, 0x0, 0x0, 0x0, false, true); // HSYNC
			else                       
				pout = VGAPixel(true, 0x0, 0x0, 0x0, false, false);
			generated_pixels++;
			if (PIXELS_TO_GENERATE == generated_pixels) {
				generated_pixels = 0;
				generated_lines++;
				if (TOP_LINES_TO_GENERATE < generated_lines) {
					state = GEN_LINES;
					generated_lines = 0;
					generated_pixels = 0;
				}
			}
			break;
		case GEN_LINES: // generate active lines	
			//cout << "video_generator: " << "GEN_LINES" << endl;
			generated_pixels++;
			if (active_pixel(generated_pixels)) {
				p = imageIn.pixel(generated_lines, generated_pixels-FRONT_BLANK_PIXELS-1);
			} else {
				p = Pixel(0, 0, 0);
			}
			hs = (1 == generated_pixels) ? true : false;
			pout = VGAPixel(true, p.red, p.green, p.blue, false, hs);
			if (PIXELS_TO_GENERATE == generated_pixels) {
				generated_pixels = 0;
				generated_lines++;
				if (LINES_TO_GENERATE == generated_lines) {
					state = GEN_BOTTOM_LINES;
					generated_lines = 0;
					generated_pixels = 0;
				}
			}
			break;
		case GEN_BOTTOM_LINES: // generate bottom blank lines
			//cout << "video_generator: " << "GEN_BOTTOM_LINES" << endl;
			if (0 == generated_pixels) pout = VGAPixel(true, 0x0, 0x0, 0x0, false, true); // HSYNC
			else                       pout = VGAPixel(true, 0x0, 0x0, 0x0, false, false);
			generated_pixels++;
			if (PIXELS_TO_GENERATE == generated_pixels) {
				generated_pixels = 0;
				generated_lines++;
				if (BOTTOM_LINES_TO_GENERATE == generated_lines) {
					state = GEN_VSYNC;
					generated_lines = 0;
					generated_pixels = 0;
				}
			}
			break;
		}
		//cout << "video_generator: " << pout << endl;
		return pout;
	}
	void video_generator_thread(){
		sc_time period(int(PIXEL_PERIOD * 1000000000), SC_NS);
		//cout << "pixel period=" << period << endl;
		for(;;){
			wait(period);
			pixel->write(video_gen_pixel());
		}
	}
	SC_CTOR(video_generator) {
		SC_THREAD(video_generator_thread);
	}
};

SC_MODULE(image_generator) {
    sc_port<sc_signal_in_if<VGAPixel> > pixel;
	void image_generator_method()
	{
		enum STATE {WAIT_VSYNC, WAIT_1RST_HSYNC, DROP_TOP_LINES, PROCESS_LINES, DROP_FRONT_PIXELS, PROCESS_PIXELS};
		static STATE state = WAIT_VSYNC; // init state of state machine
		static int dropped_lines, dropped_pixels, processed_lines, processed_pixels;
		static int image_pixel;
		bool result = false;
		static VGAimage imageOut;

		VGAPixel p = pixel->read();
		if (p.valid_data()) {
			//cout << "image_generator " << p << endl;
			switch (state) {
			case WAIT_VSYNC: // wait frame synchro
				//cout << "image_generator " << "WAIT_VSYNC" << endl;
				image_pixel = 0; // reset frame buffer pixel position
				dropped_lines = 0;
				processed_lines = 0;
				if (p.vsync()) { // VSYNC
					//imageOut.dump();
 					imageOut = imageIn; // make a copy of input image to get headers
					//imageOut.dump();
					if (p.hsync()) { // VSYNC & HSYNC
						dropped_lines = 1;
						if (TOP_LINES_TO_DROP == dropped_lines) {
							state = PROCESS_LINES;
						} else {
							state = DROP_TOP_LINES;
						}
					} else { // VSYNC alone
						state = WAIT_1RST_HSYNC;
					}
				}
				break;
			case WAIT_1RST_HSYNC: // wait 1rst line synchro, immediately after VSYNC
				//cout << "image_generator " << "WAIT_1RST_HSYNC" << endl;
				processed_lines = 0;
				if (p.hsync()) { // HSYNC
					dropped_lines = 1;
					if (TOP_LINES_TO_DROP == dropped_lines) {
						state = PROCESS_LINES;
					} else {
						state = DROP_TOP_LINES;
					}
				}
				break;
			case DROP_TOP_LINES:
				//cout << "image_generator " << "DROP_TOP_LINES" << endl;
				processed_lines = 0;
				if (p.hsync()) { // HSYNC
					dropped_lines++;
					if (TOP_LINES_TO_DROP == dropped_lines) {
						state = PROCESS_LINES;
						processed_lines = 0;
					}
				}
				break;
			case PROCESS_LINES:
				//cout << "image_generator " << "PROCESS_LINES" << endl;
				dropped_pixels = 0;
				if (p.hsync()) {
					processed_lines++;
					if (LINES_TO_PROCESS < processed_lines) {
						state = WAIT_VSYNC;
						imageOut.dumpFile("imageOut.bmp");
						result = true; // signal only once the image is ready
					} else {
						state = DROP_FRONT_PIXELS;
						dropped_pixels = 1;
					}
				}
				break;

			case DROP_FRONT_PIXELS:
				//cout << "image_generator " << "DROP_FRONT_PIXELS" << endl;
				if (FRONT_PIXELS_TO_DROP == dropped_pixels) {
					state = PROCESS_PIXELS;
					processed_pixels = 0;
				} else {
					dropped_pixels++;
				}
				break;

			case PROCESS_PIXELS:
				//cout << "image_generator " << "PROCESS_PIXELS" << endl;
				imageOut.setPixel(p.pixel(), processed_lines-1, processed_pixels);
				//printf("pass through\n");
				processed_pixels++;
				if (PIXELS_TO_PROCESS == processed_pixels) {
					state = PROCESS_LINES;
				}
				break;

			default:
				//cout << "image_generator " << "default" << endl;
				break;

			}
		}
	}
	SC_CTOR(image_generator) {
		SC_METHOD(image_generator_method);
		sensitive << pixel;
	}
};

#endif // __VIDEO_TEST_H__
