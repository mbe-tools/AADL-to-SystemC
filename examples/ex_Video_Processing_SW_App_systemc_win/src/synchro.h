#ifndef __SYNCHRO_H__
#define __SYNCHRO_H__

#include "VGAtypes.h"
#include <systemc>
using namespace std;

SC_MODULE(My_Synchro) {

    sc_port<sc_signal_in_if<bool> > capture_done;
    sc_port<sc_signal_in_if<bool> > processing_done;
    sc_port<sc_signal_in_if<bool> > display_done;
    sc_port<sc_signal_in_if<bool> > capture_ready;
    sc_port<sc_signal_in_if<bool> > processing_ready;
    sc_port<sc_signal_in_if<bool> > display_ready;
    sc_port<sc_signal_out_if<ImageId> > capture_start;
    sc_port<sc_signal_out_if<ImageId> > processing_start;
    sc_port<sc_signal_out_if<ImageId> > display_start;

	SC_CTOR(My_Synchro) {SC_THREAD(My_Synchro_code);}

	void My_Synchro_code() {
		for(;;){ 
			Pixel *addr[3] = {IMAGE1, IMAGE2, IMAGE3};
			int index;
			// wait for all blocks to be ready
			bool e, capture, processing, display;
			capture = processing = display = false;
			cout << "Synchro: " << "waiting for all ready" << endl;
			do {
				//cout << "Synchro: " << "waiting for all ready" << endl;
				wait(capture_ready->default_event() | processing_ready->default_event() | display_ready->default_event());
				if (!capture) capture = capture_ready->read();
				//cout << "capture=" << capture << endl;
				if (!processing) processing = processing_ready->read();
				//cout << "processing=" << processing << endl;
				if (!display) display = display_ready->read();
				//cout << "display=" << display << endl;
				e = capture && processing && display;
				//cout << "e=" << e << endl;
			} while (!e);
			cout << "Synchro: all blocks signaled they are ready" << endl;
			index = 0;
			// start capture of 1rst image
			cout << "Synchro: " << "signal start to capture" << endl;
			capture_start->write(ImageId(addr[index]));
			// wait for capture to be done
			capture = false;
			cout << "Synchro: " << "waiting for capture done" << endl;
			do {
				//cout << "Synchro: " << "waiting for capture done" << endl;
				wait(capture_done->default_event());
				if (!capture) capture = capture_done->read();
				//cout << "capture=" << capture << endl;
			} while (!capture);
			cout << "Synchro: " << "Capture signaled done" << endl;
			index++;
			// start capture of second image and processing of first image
			cout << "Synchro: " << "signal start to capture and processing" << endl;
			capture_start->write(ImageId(addr[index]));
			processing_start->write(ImageId(addr[index-1]));
			// wait for capture and processing to be stopped
			capture = processing = false;
			cout << "Synchro: " << "waiting for capture and processing done" << endl;
			do {
				//cout << "Synchro: " << "waiting for capture and processing done" << endl;
				wait(capture_done->default_event() | processing_done->default_event());
				if (!capture) capture = capture_done->read();
				//cout << "capture=" << capture << endl;
				if (!processing) processing = processing_done->read();
				//cout << "processing=" << processing << endl;
				e = capture && processing;
				//cout << "e=" << e << endl;
			} while (!e);
			cout << "Synchro: " << "Capture and Processing signaled done" << endl;
			index++;
			// now loop
			for(;;){
				// start all
				int i;
				cout << "Synchro: " << "signal start to capture, processing and display" << endl;
				capture_start->write(ImageId(addr[index]));
				i = index-1; processing_start->write(ImageId(addr[i < 0 ? i+3 : i]));
				i = index-2; display_start->write(ImageId(addr[i < 0 ? i+3 : i]));
				// wait all blocks
				capture = processing = display = false;
				cout << "Synchro: " << "waiting for all done" << endl;
				do {
					//cout << "Synchro: " << "waiting for all done" << endl;
					wait(capture_done->default_event() | processing_done->default_event() | display_done->default_event());
					if (!capture) capture = capture_done->read();
					//cout << "capture=" << capture << endl;
					if (!processing) processing = processing_done->read();
					//cout << "processing=" << processing << endl;
					if (!display) display = display_done->read();
					//cout << "display=" << display << endl;
					e = capture && processing && display;
					//cout << "e=" << e << endl;
				} while (!e);
			cout << "Synchro: " << "Capture, Processing and Display signaled done" << endl;
				index++; if (index > 2) index -= 3;
			}
		}
	}
};

#endif // __SYNCHRO_H__