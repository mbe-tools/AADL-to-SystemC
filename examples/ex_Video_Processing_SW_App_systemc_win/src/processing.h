#ifndef __PROCESSING_H__
#define __PROCESSING_H__

#include "VGAtypes.h"
#include <systemc.h>
using namespace std;

SC_MODULE(My_Processing) {
    sc_port<sc_signal_out_if<bool> > done;
    sc_port<sc_signal_out_if<bool> > ready;
    sc_port<sc_signal_in_if<ImageId> > start;
	void My_Processing_code() {
		done->write(false);
		//cout << "Processing: " << "signaling ready" << endl;
		ready->write(true);
		for(;;){
			// wait for start signal
			//cout << "Processing: " << "waiting for start" << endl;
			wait(start->default_event());
			ImageId imageAddr = start->read();
			done->write(false);
			int i;
			Pixel *base = imageAddr.pixelAddr();
			//cout << "Processing: " << "base=" << hex << base << dec << endl;
			for(i=0; i < PIXELS_PER_IMAGE; i++) {
				Pixel p = *base;
				base->red   = 0xFF - p.red;   //~p.red;   //p.green;
				base->green = 0xFF - p.green; //~p.green; //p.blue;
				base->blue  = 0xFF - p.blue;  //~p.blue;  //p.red;
				base++;
			}
			//cout << "Processing: " << "signaling done" << endl;
			done->write(true);
		}
	}
	SC_CTOR(My_Processing) {SC_THREAD(My_Processing_code);}
};

#endif // __PROCESSING_H__
