#ifndef __VIDEO_PARAMS_H__
#define __VIDEO_PARAMS_H__

#include "VGAtypes.h"
using namespace std;

// These VGA image format constants come from the Lena picture (256x256).
#define	   C_H_SYNC         1 //96;
#define    C_H_BACK_PORCH   0 //40;
#define    C_H_LEFT_BORDER  0 //14;
#define    C_H_VIDEO        256 //640;
#define    C_H_RIGHT_BORDER 1 //8;
#define    C_H_FRONT_PORCH  0 //2;
// default VGA vertical spec for AD9980
#define    C_V_SYNC         1 //2;
#define    C_V_BACK_PORCH   0 //25;
#define    C_V_LEFT_BORDER  0 //8;
#define    C_V_VIDEO        256 //480;
#define    C_V_RIGHT_BORDER 1 //8;
#define    C_V_FRONT_PORCH  0 //2;
// for VGA
#define    HBP          (C_H_SYNC + C_H_BACK_PORCH + C_H_LEFT_BORDER)
#define    HFP          (C_H_RIGHT_BORDER + C_H_FRONT_PORCH)
#define    PIXELS       (HBP + C_H_VIDEO + HFP)
#define    VBP          (C_V_SYNC + C_V_BACK_PORCH + C_V_LEFT_BORDER)
#define    VFP          (C_V_RIGHT_BORDER + C_V_FRONT_PORCH)
#define    LINES        (VBP + C_V_VIDEO + VFP)

// Constants for video processing.
#define TOP_LINES_TO_DROP        VBP
#define FRONT_PIXELS_TO_DROP     HBP
#define LINES_TO_PROCESS         C_V_VIDEO
#define PIXELS_TO_PROCESS        C_H_VIDEO
#define BACK_PIXELS_TO_DROP      HFP
// For video test, we need a video generator
#define PIXELS_TO_GENERATE       PIXELS
#define TOP_LINES_TO_GENERATE    VBP
#define LINES_TO_GENERATE        C_V_VIDEO
#define BOTTOM_LINES_TO_GENERATE VFP
#define FRONT_BLANK_PIXELS       HBP
#define ACTIVE_PIXELS            C_H_VIDEO
// Active area
#define ACTIVE_PIXELS_PER_LINE   C_H_VIDEO
#define ACTIVE_LINES_PER_IMAGE   C_V_VIDEO
#define PIXELS_PER_IMAGE         (ACTIVE_PIXELS_PER_LINE * ACTIVE_LINES_PER_IMAGE)
#define IMAGES_PER_SECOND		 25
#define ALL_PIXELS_PER_IMAGE	 (PIXELS*LINES)
#define PIXELS_PER_SECOND		 (IMAGES_PER_SECOND * ALL_PIXELS_PER_IMAGE)
#define PIXEL_PERIOD			 (1.0/double(PIXELS_PER_SECOND))
#define IMAGE_SIMULATION_LEN	 (PIXEL_PERIOD * ALL_PIXELS_PER_IMAGE)
#define SIMULATION_LEN			 (10.0*IMAGE_SIMULATION_LEN)

// Depends on the image format: found in video_params.
inline bool active_pixel(int pixel_pos) // 1 <= pixel_pos
{
	// if the pixel is in the image blank start, it is not active
	if (pixel_pos <= FRONT_BLANK_PIXELS) return false;

	// else the pixel is active if it is in the active area
	pixel_pos -= FRONT_BLANK_PIXELS;
	return pixel_pos <= ACTIVE_PIXELS;
}

// Constants for the global image array.
#define IMAGES 3
#define PIXELS_IN_ARRAY (PIXELS_PER_IMAGE * IMAGES)
extern Pixel globalImageArray[];
#define IMAGE1 (&globalImageArray[0])
#define IMAGE2 (&globalImageArray[PIXELS_PER_IMAGE])
#define IMAGE3 (&globalImageArray[PIXELS_PER_IMAGE*2])

#endif // __VIDEO_PARAMS_H__
