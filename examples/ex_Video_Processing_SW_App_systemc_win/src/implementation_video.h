#ifndef __IMPLEMENTATION_VIDEO_H__
#define __IMPLEMENTATION_VIDEO_H__

// get all embedded blocks
#include "capture.h"
#include "display.h"
#include "processing.h"
#include "synchro.h"
// build full system from SystemC model generated from AADL spec.
#include "video_processing_sw_app.h"

#endif // __IMPLEMENTATION_VIDEO_H__

