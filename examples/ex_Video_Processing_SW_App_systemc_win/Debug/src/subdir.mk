################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Video_Processing_SW_App_SystemC.cpp 

CPP_DEPS += \
./src/Video_Processing_SW_App_SystemC.d 

OBJS += \
./src/Video_Processing_SW_App_SystemC.o 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp src/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cygwin C++ Compiler'
	g++ -I"/cygdrive/c/systemc-2.2.0/include" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-src

clean-src:
	-$(RM) ./src/Video_Processing_SW_App_SystemC.d ./src/Video_Processing_SW_App_SystemC.o

.PHONY: clean-src

